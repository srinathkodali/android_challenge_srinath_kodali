package com.example.tech.flightcheck;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import com.example.tech.flightcheck.activities.IndividualFlightActivity;
import com.example.tech.flightcheck.adapters.FlightsAdapter;
import com.example.tech.flightcheck.constants.ServerConstants;
import com.example.tech.flightcheck.interfaces.IRecyclerViewPosition;
import com.example.tech.flightcheck.models.Flight;
import com.example.tech.flightcheck.models.FlightsMain;
import com.example.tech.flightcheck.network.CachedNetworkResponse;
import com.example.tech.flightcheck.network.GenericRequest;
import com.example.tech.flightcheck.network.VolleyErrorHelper;
import com.example.tech.flightcheck.network.VolleySingleton;
import java.util.Collections;
import java.util.Comparator;
import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;
import jp.wasabeef.recyclerview.animators.FadeInAnimator;

import static com.example.tech.flightcheck.constants.ServerConstants.EACH_FLIGHT_VARIABLE;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    ProgressDialog progressDialog;

    FlightsMain flightModel;
    String TAG = getClass().getSimpleName();

    FlightsAdapter flightsAdapter;

    public static final int ANIMATION_DURATION = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.navigation_layout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.white));

        progressDialog = new ProgressDialog(this);
        setSortFilterUICallbacks();
        if(savedInstanceState ==null )
            makeApiForData();
//        else



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void setSortFilterUICallbacks() {

        final TextView cheap = (TextView) findViewById(R.id.tv_cheap);
        final TextView early = (TextView) findViewById(R.id.tv_early);
        cheap.setTextColor(ContextCompat.getColor(this, R.color.btn_color));
        early.setTextColor(ContextCompat.getColor(getBaseContext(), android.R.color.darker_gray));

        cheap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sortByPrice();
                cheap.setTextColor(ContextCompat.getColor(getBaseContext(), R.color.btn_color));
                early.setTextColor(ContextCompat.getColor(getBaseContext(), android.R.color.darker_gray));
            }
        });


        early.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sortByTime();
                cheap.setTextColor(ContextCompat.getColor(getBaseContext(), android.R.color.darker_gray));
                early.setTextColor(ContextCompat.getColor(getBaseContext(), R.color.btn_color));
            }
        });
    }



    // make a network api call to retrive data
    public void makeApiForData() {
        String api_url = ServerConstants.BASE_URL;

        progressDialog.setMessage("Processing");
        progressDialog.setCancelable(false);
        if(!progressDialog.isShowing())
            progressDialog.show();

        CachedNetworkResponse cachedNetworkResponse = new CachedNetworkResponse(api_url, FlightsMain.class);
        if (cachedNetworkResponse.isCached()) {
            FlightsMain response = (FlightsMain) cachedNetworkResponse.getCache();
            createListOfModels(response);
        }
        else {
            GenericRequest<FlightsMain> request = new GenericRequest<FlightsMain>(api_url, FlightsMain.class, new Response.Listener<FlightsMain>() {
                @Override
                public void onResponse(final FlightsMain response) {

                    try {
                        createListOfModels(response);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    closeProgressBar();

                }

            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.d("Error.Response", String.valueOf(error));
                            closeProgressBar();
                            Toast.makeText(getBaseContext(),
                                    VolleyErrorHelper.getMessage(error, getBaseContext()),
                                    Toast.LENGTH_LONG).show();

                        }
                    }
            );


            request.setRetryPolicy(new DefaultRetryPolicy(
                    60000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


            request.setShouldCache(true);

            VolleySingleton.getInstance().addToRequestQueue(request);


        }
    }


    // creating list of models with json from api
    private void createListOfModels(FlightsMain response) {

        flightModel = response;

        Log.i(TAG, "createListOfModels: "+ flightModel);

        setRecylerView();

    }


    // setting recylerview properties on scroll and animation
    private void setRecylerView(){

        flightsAdapter = new FlightsAdapter(flightModel.getFlights());
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        sortByPrice();

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
//        recyclerView.setItemAnimator(new DefaultItemAnimator());


        IRecyclerViewPosition iRecyclerViewPosition = new IRecyclerViewPosition() {
            @Override
            public void onClickPosition(int position) {
                Intent in = new Intent(getBaseContext(), IndividualFlightActivity.class);
                Bundle b = new Bundle();

                b.putParcelable(EACH_FLIGHT_VARIABLE ,  flightModel.getFlights().get(position)  );

                in.putExtras(b);

                startActivity(in);
            }
        };
        FadeInAnimator animator = new FadeInAnimator();
        animator.setAddDuration(ANIMATION_DURATION);
        animator.setRemoveDuration(ANIMATION_DURATION);
        recyclerView.setItemAnimator(animator);

        flightsAdapter.setiRecyclerViewPosition(iRecyclerViewPosition);
        AlphaInAnimationAdapter alphaAdapter = new AlphaInAnimationAdapter(flightsAdapter);
        alphaAdapter.setDuration(1000);
        recyclerView.setAdapter(new ScaleInAnimationAdapter(alphaAdapter));



        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

        // handling scroll and adding multiple objects
        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                try {
                    final int currentFirstVisibleItem = linearLayoutManager.findFirstVisibleItemPosition();
                    int totalItemCount = linearLayoutManager.getItemCount();
                    int lastVisibleItem = linearLayoutManager.findLastCompletelyVisibleItemPosition();


//                    Log.i(TAG, "onScrolled: lastVisibleItem "+ lastVisibleItem + " , currentFirstVisibleItem "+currentFirstVisibleItem + " ,  totalItemCount "+totalItemCount);
                    if (lastVisibleItem > currentFirstVisibleItem) {
                        int lastInScreen = linearLayoutManager.findLastVisibleItemPosition();


                        if ((lastInScreen == totalItemCount - 1) ) {
////]
                        }
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

    }


    // function sorting the objects aplhabetically based on title
    private void sortByPrice(){

        Collections.sort(flightModel.getFlights(), new Comparator<Flight>() {
            @Override
            public int compare(Flight o1, Flight o2) {
                return o1.getFares().get(0).getFare() > o2.getFares().get(0).getFare() ? 1 : -1;     // sort based on title names of objects
            }
        });

        flightsAdapter.notifyDataSetChanged();

    }
    private void sortByTime(){

        Collections.sort(flightModel.getFlights(), new Comparator<Flight>() {
            @Override
            public int compare(Flight o1, Flight o2) {
                return o1.getDepartureTime() > o2.getDepartureTime() ? 1 : -1;     // sort based on title names of objects
            }
        });

        flightsAdapter.notifyDataSetChanged();

    }




    // close progressdialogbox
    private void closeProgressBar() {

        if(progressDialog.isShowing())
            progressDialog.dismiss();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("obj", flightModel);
    }


    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onRestoreInstanceState(savedInstanceState);
        flightModel=savedInstanceState.getParcelable("obj");
        if(flightModel ==null )
            makeApiForData();
        else
            setRecylerView();

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button

        int id = item.getItemId();


        if (id == R.id.nav_share) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        Log.i(TAG, "onNavigationItemSelected: item "+item.getItemId());

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
