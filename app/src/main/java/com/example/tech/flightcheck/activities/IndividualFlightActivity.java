package com.example.tech.flightcheck.activities;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.tech.flightcheck.R;
import com.example.tech.flightcheck.adapters.BookingPlatformAdapter;
import com.example.tech.flightcheck.interfaces.IRecyclerViewPosition;
import com.example.tech.flightcheck.models.Fare;
import com.example.tech.flightcheck.models.Flight;

import java.util.Collections;
import java.util.Comparator;

import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;
import jp.wasabeef.recyclerview.animators.FadeInAnimator;

import static com.example.tech.flightcheck.constants.ServerConstants.EACH_FLIGHT_VARIABLE;

public class IndividualFlightActivity extends AppCompatActivity {

    public static final int ANIMATION_DURATION = 2000;
    Flight flight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_individual_flight);

        Bundle b= getIntent().getExtras();

        flight = b.getParcelable(EACH_FLIGHT_VARIABLE);


        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle("DEL -> BOM");
        mToolbar.setTitleTextColor(ContextCompat.getColor(this, android.R.color.white));


        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);




    }

    private void callAllComponentMethods(){


        RecyclerView rv_booking_platforms = (RecyclerView) findViewById(R.id.rv_all_booking_options);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        rv_booking_platforms.setLayoutManager(mLayoutManager);
//        recyclerView.setItemAnimator(new DefaultItemAnimator());

        rv_booking_platforms.setNestedScrollingEnabled(false);

        FadeInAnimator animator = new FadeInAnimator();
        animator.setAddDuration(ANIMATION_DURATION);
        animator.setRemoveDuration(ANIMATION_DURATION);
        rv_booking_platforms.setItemAnimator(animator);


        BookingPlatformAdapter bookingPlatformAdapter = new BookingPlatformAdapter(flight.getFares());


        AlphaInAnimationAdapter alphaAdapter = new AlphaInAnimationAdapter(bookingPlatformAdapter);
        alphaAdapter.setDuration(1000);
        rv_booking_platforms.setAdapter(new ScaleInAnimationAdapter(alphaAdapter));

        setFlightSourceDestination();

        setMinBookingLayout();
    }

    private void setFlightSourceDestination() {
        ImageView im_flight_lane = (ImageView) findViewById(R.id.im_selected_flight_lane);
        if (flight.getAirlineCode().equals("6E"))
            im_flight_lane.setImageResource(R.drawable.indigo);
        else if (flight.getAirlineCode().equals("SG"))
            im_flight_lane.setImageResource(R.drawable.sg);
        else if (flight.getAirlineCode().equals("G8"))
            im_flight_lane.setImageResource(R.drawable.g8);
        else if (flight.getAirlineCode().equals("9W"))
            im_flight_lane.setImageResource(R.drawable.jet);
        else if (flight.getAirlineCode().equals("AI"))
            im_flight_lane.setImageResource(R.drawable.ai);


        TextView tv_name = (TextView) findViewById(R.id.tv_airlane_name);

        tv_name.setText( flight.getAirlineCode() + " - " + flight.getClass_());

        TextView tv_duration = (TextView) findViewById(R.id.tv_destination_travel_time);

        tv_duration.setText(flight.getDurationTimeDateFormat());

        TextView sourcePlace = (TextView) findViewById(R.id.tv_departure_time);
        sourcePlace.setText("DEL "+flight.getDepartureTimeDateFormat());

        TextView destinationPlace = (TextView) findViewById(R.id.tv_arrival_time);
        destinationPlace.setText(("BOM "+flight.getArrivalTimeDateFormat()));

        TextView destinationDay = (TextView) findViewById(R.id.tv_arrival_day);
        destinationDay.setText((flight.getArrivalTimeDateDayFormat()));


        TextView departureDay = (TextView) findViewById(R.id.tv_departure_day);
        departureDay.setText((flight.getDepartureTimeDateDayFormat()));

    }

    private void setMinBookingLayout(){
        TextView minvalue= (TextView) findViewById(R.id.tv_min_book_amount);

        Collections.sort(flight.getFares(), new Comparator<Fare>() {
            @Override
            public int compare(Fare o1, Fare o2) {
                return o1.getFare() > o2.getFare() ? 1 : -1;     // sort based on title names of objects
            }
        });

        minvalue.setText("₹"+flight.getFares().get(0).getFare());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("obj", flight);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onRestoreInstanceState(savedInstanceState);
        flight = savedInstanceState.getParcelable("obj");
        if (flight == null){
            Bundle b = getIntent().getExtras();
            flight = b.getParcelable(EACH_FLIGHT_VARIABLE);
        }
//        else
            callAllComponentMethods();

    }
}
