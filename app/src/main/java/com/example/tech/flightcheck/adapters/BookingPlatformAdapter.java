package com.example.tech.flightcheck.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.tech.flightcheck.R;
import com.example.tech.flightcheck.interfaces.IRecyclerViewPosition;
import com.example.tech.flightcheck.models.Fare;
import com.example.tech.flightcheck.models.Flight;
import com.example.tech.flightcheck.views.BookingPlatformView;
import com.example.tech.flightcheck.views.FlightView;

import java.util.List;

/**
 * Created by tech on 22/9/17.
 */

public class BookingPlatformAdapter extends RecyclerView.Adapter<BookingPlatformView> {

    private List<Fare> projectList;


    public BookingPlatformAdapter(List<Fare> projectList) {
        this.projectList = projectList;
    }

    @Override
    public BookingPlatformView onCreateViewHolder(final ViewGroup parent, final int position) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.each_booking_layout, parent, false);

        return new BookingPlatformView(itemView);
    }

    @Override
    public void onBindViewHolder(BookingPlatformView holder, int position) {
        try {
            Fare fare = projectList.get(position);
            holder.fare_price.setText("₹"+fare.getFare());
//
//            holder.departureTime.setText(flight.getDepartureTimeDateFormat().toString());
//            holder.duration.setText(flight.getDurationTimeDateFormat().toString());
//            holder.fare.setText("₹"+flight.getFares().get(0).getFare());
//

            if (fare.getProviderId() == 1)
                holder.im_platform.setImageResource(R.drawable.mmtlogo);
            else if (fare.getProviderId() == 2 )
                holder.im_platform.setImageResource(R.drawable.clear);
            else if (fare.getProviderId() == 3 )
                holder.im_platform.setImageResource(R.drawable.yatra);
            else if (fare.getProviderId() == 4 )
                holder.im_platform.setImageResource(R.drawable.musafir);

        }catch (Exception e){
            Log.i(getClass().getSimpleName(), "onBindViewHolder: "+position);
            e.printStackTrace();
        }

    }


    @Override
    public int getItemCount() {
        return projectList.size();
    }
}