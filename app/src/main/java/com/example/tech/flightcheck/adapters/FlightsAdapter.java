package com.example.tech.flightcheck.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.tech.flightcheck.R;
import com.example.tech.flightcheck.interfaces.IRecyclerViewPosition;
import com.example.tech.flightcheck.models.Flight;
import com.example.tech.flightcheck.views.FlightView;

import java.util.List;

/**
 * Created by tech on 13/8/17.
 */

public class FlightsAdapter extends RecyclerView.Adapter<FlightView> {

    private List<Flight> projectList;

    IRecyclerViewPosition iRecyclerViewPosition;


    public void setiRecyclerViewPosition(IRecyclerViewPosition iRecyclerViewPosition) {
        this.iRecyclerViewPosition = iRecyclerViewPosition;
    }



    public FlightsAdapter(List<Flight> projectList) {
        this.projectList = projectList;
    }

    @Override
    public FlightView onCreateViewHolder(final ViewGroup parent, final int position) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.each_flight_layout, parent, false);


        return new FlightView(itemView);
    }

    @Override
    public void onBindViewHolder(FlightView holder, final int position) {
        try {
            Flight flight = projectList.get(position);
            holder.arrivalTime.setText(flight.getArrivalTimeDateFormat().toString());

            holder.departureTime.setText(flight.getDepartureTimeDateFormat().toString());
            holder.duration.setText(flight.getDurationTimeDateFormat().toString());
            holder.fare.setText("₹"+flight.getFares().get(0).getFare());

            holder.airlane_code.setText(flight.getAirlineCode());
            if (flight.getAirlineCode().equals("6E"))
                holder.im_airlane.setImageResource(R.drawable.indigo);
            else if (flight.getAirlineCode().equals("SG"))
                holder.im_airlane.setImageResource(R.drawable.sg);
            else if (flight.getAirlineCode().equals("G8"))
                holder.im_airlane.setImageResource(R.drawable.g8);
            else if (flight.getAirlineCode().equals("9W"))
                holder.im_airlane.setImageResource(R.drawable.jet);
            else if (flight.getAirlineCode().equals("AI"))
                holder.im_airlane.setImageResource(R.drawable.ai);

            holder.rl_complete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    iRecyclerViewPosition.onClickPosition(position);
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }

    }


    @Override
    public int getItemCount() {
        return projectList.size();
    }
}