package com.example.tech.flightcheck.constants;

/**
 * Created by tech on 13/8/17.
 */

public class ServerConstants {
    public static final String BASE_URL = "http://www.mocky.io/v2/5979c6731100001e039edcb3";
    public static final String EACH_FLIGHT_VARIABLE = "flights";
}
