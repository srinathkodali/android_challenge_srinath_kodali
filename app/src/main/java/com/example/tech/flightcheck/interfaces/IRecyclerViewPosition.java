package com.example.tech.flightcheck.interfaces;

/**
 * Created by tech on 13/8/17.
 */

public interface IRecyclerViewPosition {

    public void onClickPosition(int position);
}
