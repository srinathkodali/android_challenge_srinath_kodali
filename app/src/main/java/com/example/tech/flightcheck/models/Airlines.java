
package com.example.tech.flightcheck.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Airlines implements Parcelable
{

    @SerializedName("SG")
    @Expose
    private String sG;
    @SerializedName("AI")
    @Expose
    private String aI;
    @SerializedName("G8")
    @Expose
    private String g8;
    @SerializedName("9W")
    @Expose
    private String _9W;
    @SerializedName("6E")
    @Expose
    private String _6E;
    public final static Parcelable.Creator<Airlines> CREATOR = new Creator<Airlines>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Airlines createFromParcel(Parcel in) {
            Airlines instance = new Airlines();
            instance.sG = ((String) in.readValue((String.class.getClassLoader())));
            instance.aI = ((String) in.readValue((String.class.getClassLoader())));
            instance.g8 = ((String) in.readValue((String.class.getClassLoader())));
            instance._9W = ((String) in.readValue((String.class.getClassLoader())));
            instance._6E = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public Airlines[] newArray(int size) {
            return (new Airlines[size]);
        }

    }
    ;

    public String getSG() {
        return sG;
    }

    public void setSG(String sG) {
        this.sG = sG;
    }

    public String getAI() {
        return aI;
    }

    public void setAI(String aI) {
        this.aI = aI;
    }

    public String getG8() {
        return g8;
    }

    public void setG8(String g8) {
        this.g8 = g8;
    }

    public String get9W() {
        return _9W;
    }

    public void set9W(String _9W) {
        this._9W = _9W;
    }

    public String get6E() {
        return _6E;
    }

    public void set6E(String _6E) {
        this._6E = _6E;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(sG);
        dest.writeValue(aI);
        dest.writeValue(g8);
        dest.writeValue(_9W);
        dest.writeValue(_6E);
    }

    public int describeContents() {
        return  0;
    }

}
