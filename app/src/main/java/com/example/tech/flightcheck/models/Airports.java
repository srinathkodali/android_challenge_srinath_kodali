
package com.example.tech.flightcheck.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Airports implements Parcelable
{

    @SerializedName("DEL")
    @Expose
    private String dEL;
    @SerializedName("BOM")
    @Expose
    private String bOM;
    public final static Parcelable.Creator<Airports> CREATOR = new Creator<Airports>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Airports createFromParcel(Parcel in) {
            Airports instance = new Airports();
            instance.dEL = ((String) in.readValue((String.class.getClassLoader())));
            instance.bOM = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public Airports[] newArray(int size) {
            return (new Airports[size]);
        }

    }
    ;

    public String getDEL() {
        return dEL;
    }

    public void setDEL(String dEL) {
        this.dEL = dEL;
    }

    public String getBOM() {
        return bOM;
    }

    public void setBOM(String bOM) {
        this.bOM = bOM;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(dEL);
        dest.writeValue(bOM);
    }

    public int describeContents() {
        return  0;
    }

}
