
package com.example.tech.flightcheck.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Appendix implements Parcelable
{

    @SerializedName("airlines")
    @Expose
    private Airlines airlines;
    @SerializedName("airports")
    @Expose
    private Airports airports;
    @SerializedName("providers")
    @Expose
    private Providers providers;
    public final static Parcelable.Creator<Appendix> CREATOR = new Creator<Appendix>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Appendix createFromParcel(Parcel in) {
            Appendix instance = new Appendix();
            instance.airlines = ((Airlines) in.readValue((Airlines.class.getClassLoader())));
            instance.airports = ((Airports) in.readValue((Airports.class.getClassLoader())));
            instance.providers = ((Providers) in.readValue((Providers.class.getClassLoader())));
            return instance;
        }

        public Appendix[] newArray(int size) {
            return (new Appendix[size]);
        }

    }
    ;

    public Airlines getAirlines() {
        return airlines;
    }

    public void setAirlines(Airlines airlines) {
        this.airlines = airlines;
    }

    public Airports getAirports() {
        return airports;
    }

    public void setAirports(Airports airports) {
        this.airports = airports;
    }

    public Providers getProviders() {
        return providers;
    }

    public void setProviders(Providers providers) {
        this.providers = providers;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(airlines);
        dest.writeValue(airports);
        dest.writeValue(providers);
    }

    public int describeContents() {
        return  0;
    }

}
