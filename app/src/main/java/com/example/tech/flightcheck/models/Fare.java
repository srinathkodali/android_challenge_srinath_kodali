
package com.example.tech.flightcheck.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Fare implements Parcelable
{

    @SerializedName("providerId")
    @Expose
    private long providerId;
    @SerializedName("fare")
    @Expose
    private long fare;
    public final static Parcelable.Creator<Fare> CREATOR = new Creator<Fare>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Fare createFromParcel(Parcel in) {
            Fare instance = new Fare();
            instance.providerId = ((long) in.readValue((long.class.getClassLoader())));
            instance.fare = ((long) in.readValue((long.class.getClassLoader())));
            return instance;
        }

        public Fare[] newArray(int size) {
            return (new Fare[size]);
        }

    }
    ;

    public long getProviderId() {
        return providerId;
    }

    public void setProviderId(long providerId) {
        this.providerId = providerId;
    }

    public long getFare() {
        return fare;
    }

    public void setFare(long fare) {
        this.fare = fare;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(providerId);
        dest.writeValue(fare);
    }

    public int describeContents() {
        return  0;
    }

}
