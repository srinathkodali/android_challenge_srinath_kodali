
package com.example.tech.flightcheck.models;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.joda.time.DateTime;
import org.joda.time.Period;

import static com.example.tech.flightcheck.constants.NameConstants.DATE_FORMAT_PATTERN;

public class Flight implements Parcelable
{

    @SerializedName("originCode")
    @Expose
    private String originCode;
    @SerializedName("destinationCode")
    @Expose
    private String destinationCode;
    @SerializedName("departureTime")
    @Expose
    private long departureTime;
    @SerializedName("arrivalTime")
    @Expose
    private long arrivalTime;
    @SerializedName("fares")
    @Expose
    private List<Fare> fares = null;
    @SerializedName("airlineCode")
    @Expose
    private String airlineCode;
    @SerializedName("class")
    @Expose
    private String _class;
    public final static Parcelable.Creator<Flight> CREATOR = new Creator<Flight>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Flight createFromParcel(Parcel in) {
            Flight instance = new Flight();
            instance.originCode = ((String) in.readValue((String.class.getClassLoader())));
            instance.destinationCode = ((String) in.readValue((String.class.getClassLoader())));
            instance.departureTime = ((long) in.readValue((long.class.getClassLoader())));
            instance.arrivalTime = ((long) in.readValue((long.class.getClassLoader())));
//            instance.fares = in.readParcelable(Fare.class.getClassLoader());
            instance.fares = in.readArrayList( (Fare.class.getClassLoader()));
            instance.airlineCode = ((String) in.readValue((String.class.getClassLoader())));
            instance._class = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public Flight[] newArray(int size) {
            return (new Flight[size]);
        }

    }
    ;


    public String getOriginCode() {
        return originCode;
    }

    public void setOriginCode(String originCode) {
        this.originCode = originCode;
    }

    public String getDestinationCode() {
        return destinationCode;
    }

    public void setDestinationCode(String destinationCode) {
        this.destinationCode = destinationCode;
    }

    public long getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(long departureTime) {
        this.departureTime = departureTime;
    }

    public long getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(long arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public List<Fare> getFares() {
        return fares;
    }

    public void setFares(List<Fare> fares) {
        this.fares = fares;
    }

    public String getAirlineCode() {
        return airlineCode;
    }

    public void setAirlineCode(String airlineCode) {
        this.airlineCode = airlineCode;
    }

    public String getClass_() {
        return _class;
    }

    public void setClass_(String _class) {
        this._class = _class;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(originCode);
        dest.writeValue(destinationCode);
        dest.writeValue(departureTime);
        dest.writeValue(arrivalTime);
        dest.writeList(fares);
        dest.writeValue(airlineCode);
        dest.writeValue(_class);
    }

    public int describeContents() {
        return  0;
    }


    public String getArrivalTimeDateFormat(){
        return formatingTime(new Timestamp(arrivalTime));
    }
    public String getDepartureTimeDateFormat(){

        return formatingTime(new Timestamp(departureTime));
    }
    public String getDurationTimeDateFormat(){

//        DateTime startTime, endTime;
        Period p = new Period(departureTime, arrivalTime );
        long hours = p.getHours();
        long minutes = p.getMinutes();
        if(minutes > 0)
            return String.format("%2dh:%02dm", hours, minutes) ;
        else
            return String.format("%2dh", hours) ;
//        return formatingTime(((arrivalTime - departureTime)));
    }

    private String formatingTime(Timestamp time){
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_PATTERN);
        return sdf.format(time);
    }


    public String getArrivalTimeDateDayFormat() {
        SimpleDateFormat sdf = new SimpleDateFormat("EEE, MMM d");
        return sdf.format(arrivalTime);
    }
    public String getDepartureTimeDateDayFormat() {
        SimpleDateFormat sdf = new SimpleDateFormat("EEE, MMM d");
        return sdf.format(arrivalTime);
    }
}
