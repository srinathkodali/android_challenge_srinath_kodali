
package com.example.tech.flightcheck.models;

import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FlightsMain implements Parcelable
{

    @SerializedName("appendix")
    @Expose
    private Appendix appendix;
    @SerializedName("flights")
    @Expose
    private List<Flight> flights = null;
    public final static Parcelable.Creator<FlightsMain> CREATOR = new Creator<FlightsMain>() {


        @SuppressWarnings({
            "unchecked"
        })
        public FlightsMain createFromParcel(Parcel in) {
            FlightsMain instance = new FlightsMain();
            instance.appendix = ((Appendix) in.readValue((Appendix.class.getClassLoader())));
            in.readList(instance.flights, (com.example.tech.flightcheck.models.Flight.class.getClassLoader()));
            return instance;
        }

        public FlightsMain[] newArray(int size) {
            return (new FlightsMain[size]);
        }

    }
    ;

    public Appendix getAppendix() {
        return appendix;
    }

    public void setAppendix(Appendix appendix) {
        this.appendix = appendix;
    }

    public List<Flight> getFlights() {
        return flights;
    }

    public void setFlights(List<Flight> flights) {
        this.flights = flights;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(appendix);
        dest.writeList(flights);
    }

    public int describeContents() {
        return  0;
    }

}
