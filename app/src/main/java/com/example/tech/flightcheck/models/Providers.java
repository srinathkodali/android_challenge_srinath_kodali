
package com.example.tech.flightcheck.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Providers implements Parcelable
{

    @SerializedName("1")
    @Expose
    private String _1;
    @SerializedName("2")
    @Expose
    private String _2;
    @SerializedName("3")
    @Expose
    private String _3;
    @SerializedName("4")
    @Expose
    private String _4;
    public final static Parcelable.Creator<Providers> CREATOR = new Creator<Providers>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Providers createFromParcel(Parcel in) {
            Providers instance = new Providers();
            instance._1 = ((String) in.readValue((String.class.getClassLoader())));
            instance._2 = ((String) in.readValue((String.class.getClassLoader())));
            instance._3 = ((String) in.readValue((String.class.getClassLoader())));
            instance._4 = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public Providers[] newArray(int size) {
            return (new Providers[size]);
        }

    }
    ;

    public String get1() {
        return _1;
    }

    public void set1(String _1) {
        this._1 = _1;
    }

    public String get2() {
        return _2;
    }

    public void set2(String _2) {
        this._2 = _2;
    }

    public String get3() {
        return _3;
    }

    public void set3(String _3) {
        this._3 = _3;
    }

    public String get4() {
        return _4;
    }

    public void set4(String _4) {
        this._4 = _4;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(_1);
        dest.writeValue(_2);
        dest.writeValue(_3);
        dest.writeValue(_4);
    }

    public int describeContents() {
        return  0;
    }

}
