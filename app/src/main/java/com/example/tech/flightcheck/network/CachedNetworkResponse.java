package com.example.tech.flightcheck.network;


import com.android.volley.Cache;
import com.example.tech.flightcheck.network.VolleySingleton;
import com.google.gson.Gson;


/**
 * Created by tech on 9/9/17.
 */

public class CachedNetworkResponse<T> {
    private   String url;
    private   Gson gson = new Gson();
    private   Class<T> clazz=null;

    public CachedNetworkResponse(String url, Class<T> clazz) {
        this.url = url;
        this.clazz = clazz;
    }

    public void removeCache(){
        Cache cache = VolleySingleton.getInstance().getRequestQueue().getCache();
        Cache.Entry entry = cache.get(this.url);
        cache.remove(this.url);
    }


    public  String getUrl() {
        return url;
    }

    public void setUrl(String Url) {
       this.url = Url;
    }
    public  boolean isCached() {
        Cache cache = VolleySingleton.getInstance().getRequestQueue().getCache();
        Cache.Entry entry = cache.get(this.url);

        if (entry != null && !entry.isExpired()) {
            return  true;
        }
        else
            return  false;
    }


    public  T  getCache(){
        try {
            Cache cache = VolleySingleton.getInstance().getRequestQueue().getCache();
            Cache.Entry entry = cache.get(getUrl());

            if (entry != null && !entry.isExpired()) {
                byte b[] = entry.data;
                String temp = new String(b,"UTF-8");
             T parsedObject = gson.fromJson(temp, clazz);
                return  parsedObject;
            }
        }
        catch (Exception e){
            e.printStackTrace();

        }
        return  null;
    }

}
