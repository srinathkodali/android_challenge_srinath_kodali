package com.example.tech.flightcheck.network;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.example.tech.flightcheck.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by tech on 13/8/17.
 */

public class VolleyErrorHelper {
    public static String getMessage(Object error, Context context) {
        if (error instanceof TimeoutError) {
            return context.getResources().getString(R.string.errorRequestTimeout);
        } else if (isServerProblem(error)) {
            return handleServerError(error, context);
        } else if (isAuthProblem(error)) {
            return context.getResources().getString(R.string.errorAuthenticationFailure);
        } else if (isNetworkProblem(error)) {
            return context.getResources().getString(R.string.errorNoNetworkConnection);
        }

        return context.getResources().getString(R.string.errorDefault);
    }

    private static boolean isNetworkProblem(Object error) {
        return (error instanceof NetworkError)
                || (error instanceof NoConnectionError);
    }

    private static boolean isAuthProblem(Object error) {
        return (error instanceof AuthFailureError);
    }

    private static boolean isServerProblem(Object error) {
        return (error instanceof ServerError);
    }

    private static String handleServerError(Object err, Context context) {
        VolleyError error = (VolleyError) err;

        NetworkResponse response = error.networkResponse;

        if (response != null) {
            switch (response.statusCode) {

                case 401:
                    try {
                        HashMap<String, String> result = new Gson().fromJson(new String(response.data),
                                new TypeToken<Map<String, String>>() {
                                }.getType());

                        if (result != null && result.containsKey("error")) {
                            return result.get("error");
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    return error.getMessage();
                case 404:
                    return "SomeThing Went Wrong";
                case 422:
                    return "Unprocessable Response";

                default:
                    return context.getResources().getString(
                            R.string.errorServerNotReachable);
            }
        }
        return context.getResources().getString(R.string.errorDefault);
    }
}