package com.example.tech.flightcheck.network;

import android.graphics.Bitmap;
import android.support.v4.util.LruCache;

import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.example.tech.flightcheck.MyApplication;


import org.json.JSONObject;

import java.nio.charset.Charset;

/**
 * Created by tech-1 on 10/8/17.
 */

public class VolleySingleton {

    private static VolleySingleton mInstance = null;
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;
    private final LruCache<String, Bitmap> mCache = new LruCache<>(5);;

    private VolleySingleton(){

        HurlStack hurlStack =new HurlStack(null, ClientSSLSocketFactory.getSocketFactory());
        mRequestQueue = Volley.newRequestQueue(MyApplication.getAppContext(),hurlStack);
        mImageLoader = new ImageLoader(this.mRequestQueue, new LruBitmapCache());
    }

    public static VolleySingleton getInstance(){
        if(mInstance == null){
            mInstance = new VolleySingleton();
        }
        return mInstance;
    }
    public void addResponseToCache(Cache cache,String url,String response,Double time){
        try {
            Cache.Entry entry = new Cache.Entry();
            final long cacheHitButRefreshed = (long)(time*60 * 60 * 1000); // in 1 minute cache will be hit, but also refreshed on background
            final long cacheExpired = (long) (time * 60 * 60 * 1000); // in 0.5 hour this cache entry expires completely
            long now = System.currentTimeMillis();
            final long softExpire = now + cacheHitButRefreshed;
            final long ttl = now + cacheExpired;
            byte b[] = response.getBytes();
            entry.data = response.toString().getBytes("UTF-8");
            entry.etag = url;
            entry.softTtl = softExpire;
            entry.ttl = ttl;
            cache.put(url, entry);
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }


//    public void addToRequestQueue(Request request){
//        getRequestQueue().add(request);
//    }

    public <T> void  addToRequestQueue(Request<T> request){
        request.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        getRequestQueue().add(request);
    }

    public RequestQueue getRequestQueue(){
        if (mRequestQueue == null) {
            HurlStack hurlStack = new HurlStack(null, ClientSSLSocketFactory.getSocketFactory());
            mRequestQueue = Volley.newRequestQueue(MyApplication.getAppContext(), hurlStack);
        }
        return mRequestQueue;
    }

    public ImageLoader getImageLoader(){
        return this.mImageLoader;
    }

    public void clearCache(String key){
        mCache.remove(key);
    }





}