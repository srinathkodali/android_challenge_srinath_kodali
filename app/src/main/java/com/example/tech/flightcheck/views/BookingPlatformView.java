package com.example.tech.flightcheck.views;

import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorListener;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.tech.flightcheck.R;

import jp.wasabeef.recyclerview.animators.holder.AnimateViewHolder;


public class BookingPlatformView extends AnimateViewHolder {
    public TextView fare_price;
    public ImageView im_platform;

    public BookingPlatformView(View view) {
        super(view);
        fare_price = (TextView) view.findViewById(R.id.tv_platform_price);
        im_platform = (ImageView) view.findViewById(R.id.im_travel_platform);
    }

    @Override
    public void animateAddImpl(ViewPropertyAnimatorListener listener) {
        ViewCompat.animate(itemView)
                .translationY(0)
                .alpha(1)
                .setDuration(300)
                .setListener(listener)
                .start();
    }

    @Override
    public void animateRemoveImpl(ViewPropertyAnimatorListener listener) {
        ViewCompat.animate(itemView)
                .translationY(-itemView.getHeight() * 0.3f)
                .alpha(0)
                .setDuration(300)
                .setListener(listener)
                .start();
    }


}
