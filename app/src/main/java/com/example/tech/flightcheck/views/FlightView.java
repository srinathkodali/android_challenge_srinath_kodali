package com.example.tech.flightcheck.views;

import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorListener;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.tech.flightcheck.R;

import jp.wasabeef.recyclerview.animators.holder.AnimateViewHolder;

/**
 * Created by tech on 13/8/17.
 */

public class FlightView extends AnimateViewHolder {
    public TextView departureTime, arrivalTime, fare, duration, airlane_code;
    public ImageView im_airlane;
    public RelativeLayout rl_complete;

    public FlightView(View view) {
        super(view);
        departureTime = (TextView) view.findViewById(R.id.tv_departure_time);
        arrivalTime = (TextView) view.findViewById(R.id.tv_arrival_time);
        fare = (TextView) view.findViewById(R.id.tv_fare);
        duration = (TextView) view.findViewById(R.id.tv_duration);
        airlane_code = (TextView) view.findViewById(R.id.tv_airlane_code);
        im_airlane = (ImageView) view.findViewById(R.id.im_airline);
        rl_complete = (RelativeLayout) view.findViewById(R.id.rl_entire_layout);
    }

    @Override
    public void animateAddImpl(ViewPropertyAnimatorListener listener) {
        ViewCompat.animate(itemView)
                .translationY(0)
                .alpha(1)
                .setDuration(300)
                .setListener(listener)
                .start();
    }

    @Override
    public void animateRemoveImpl(ViewPropertyAnimatorListener listener) {
        ViewCompat.animate(itemView)
                .translationY(-itemView.getHeight() * 0.3f)
                .alpha(0)
                .setDuration(300)
                .setListener(listener)
                .start();
    }




}